

let userElem1 = Number(prompt("Введіть перше число", 0));
let userElem2 = Number(prompt("Введіть друге число", 0));
let userSign = prompt("Введіть знак арифметичної дії", "+");


if (isNaN(userElem1) || isNaN(userElem2)) {
    console.error("Ви ввели не число");
}
if (userElem2 == 0) {
    console.error("На нуль ділити не можна");
}
if (userSign != "+" && userSign != "-" && userSign != "*" && userSign != "/") {
    console.error("Ви ввели не знак арифметичної дії");
}

const add = (num1, num2) => {
    return num1 + num2;
}
const sub = (num1, num2) => {
    return num1 - num2;
}
const mul = (num1, num2) => {
    return num1 * num2;
}
const div = (num1, num2) => {
    return num1 / num2;
}

function calculate(elem1, elem2, callback, show) {
    

    show(callback(elem1, elem2));
}

function show(data) {
    document.getElementById("classwork7").innerHTML = `${userElem1} ${userSign} ${userElem2} = ${data}`;
}

let fn;

switch (userSign) {
    case "+":
        fn = add;
        break;
    case "-":
        fn = sub;
        break;
    case "*":
        fn = mul;
        break;
    case "/":
        fn = div;
        break;
}

calculate(userElem1, userElem2, fn, show);

/*
switch (userSign) {
    case "+":
        calculate(userElem1, userElem2, add, show)
        break;
    case "-":
        calculate(userElem1, userElem2, sub, show)
        break;
    case "*":
        calculate(userElem1, userElem2, mul, show)
        break;
    case "/":
        calculate(userElem1, userElem2, div, show)
        break;
}
*/