

const doc = {
    title: "",
    body: "",
    footer: "",
    date: "",
    app: {
        title: "",
        body: "",
        footer: "",
        date: ""
    },
    getUserInfo: function () {
        
        this.title = prompt("Запишіть заголовок");
        this.body = prompt("Запишіть інформацію");
        this.footer = prompt("Запишіть футер");
        this.date = prompt("Запишіть дату");
        this.app.title = '1556589';
        this.app.body = '844685458';
        this.app.footer = '658788568';
        this.app.date = '53458654';
        console.log(this.title);
    },
    showUserInfo: function () {
        document.getElementById('homework').innerHTML = this.getTemplate(doc)
        
    },
    getTemplate: function (obj) {
        let template = '';
        for (let key in obj) {
            if (typeof obj[key] === 'string') {
                template += `${key}: ${obj[key]} <br> `
             
            } else if (typeof this[key] === 'object') {
                const obj2 = this[key];
                for (let elem in obj2) {
                if (typeof obj2[elem] === 'string') {
                    template += `${elem}: ${obj2[elem]} <br> `
                    }
                }
            }
             
        }
        return template;             
    }
}

doc.getUserInfo();
doc.showUserInfo();


/*
function doc1(data) {
    this.title = data.title;
    this.body = data.body;
    this.footer = data.footer;
    this.date = data.date;
    this.app = data.app;

    this.getUserInfo = () => {
        console.log(this);
        return this;
    }

    this.showUserInfo = () => {
        console.log("showUserInfo", this);
        return;
    }

    this.functions = {
        
        fn1: function () {
            console.log("functions.fn1")
        },
        functions: {
            fn1: function () {
                console.log("functions.functions.fn1")
            },
        }

    }
}

const test50 = new doc1({ title: "test1", body: 1 });

//console.log(test1, test1.functions.fn1(), test1.functions.functions.fn1(), test1.functions.text80);

////2

function doc1(data) {
    this.title = data.title;
    this.body = data.body;
    this.footer = data.body;
    this.date = data.date;
    this.app = data.app;
    this.getUserInfo = () => {
        console.log(this);
        return this;
    }
    this.showUserInfo = () => {
        console.log("showUserInfo", this);
        return;
    }
    this.functions = {
    
        fn1: () => {
            console.log("Runned: functions.fn1")
        },
        functions: {
            fn1: () =>  {
                console.log("Runned:functions.functions.fn1")
            },
        }
    }
}
function test() {
    this.text = 'Hui',
    this.funcs = {
        text: 'Hui1',
        fn: () => {
            console.log('Hui1');
        }
    }
}
const test1 = new doc1({ title: "test1", body: 1, app: { title: "test3", body: 3 } });
console.log(test1);
test1.functions.fn1();
test1.functions.functions.fn1();
const fTest = new test();

*/