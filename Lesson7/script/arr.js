let arrData = ['hello', 'world', 23, '23', null];

let filterBy = (arr, type) => arr.filter(elem => typeof elem !== type);

console.log(filterBy(arrData, 'string'));
console.log(filterBy(arrData, 'number'));
console.log(filterBy(arrData, 'object')); // фільтрує null з масиву

