

// Homework
const styles = ["Джаз", "Блюз"];
styles.push("Рок-н-ролл");
if (styles.length % 2 == 0) {
    styles.splice(styles.length / 2 - 1, 1, "Классика");
} else {
    styles.splice(styles.length / 2, 1, "Классика");
}
const del = styles.shift();
console.log(del);
styles.unshift("Реп", "Регги");
console.log(styles);


// Work 1

const arr1 = ['a', 'b', 'c'];
const arr1_1 = [1, 2, 3];
const rez1 = [...arr1, ...arr1_1]
console.log(rez1);

const rez1_1 = arr1.concat(arr1_1);
console.log(rez1_1);

const rez1_2 = [].concat(arr1, arr1_1);
console.log(rez1_2);

arr1.push(...arr1_1);
console.log(arr1);

// Work 2

const arr2 = ['a', 'b', 'c'];
arr2.push(1, 2, 3);

console.log(arr2);

// Work 3

const arr3 = [1, 2, 3];
arr3.reverse();

console.log(arr3);

// Work 4

const arr4 = [1, 2, 3];
arr4.push(4, 5, 6);

console.log(arr4);

// Work 5

const arr5 = [1, 2, 3];
arr5.unshift(4, 5, 6);

console.log(arr5);

// Work 6

const arr6 = ['js', 'css', 'jq'];

console.log(arr6[0]);

// Work 7

const arr7 = [1, 2, 3, 4, 5];
const arr7_1 = arr7.slice(0, 3);

console.log(arr7_1);

// Work 8

const arr8 = [1, 2, 3, 4, 5];
arr8.splice(1, 2);

console.log(arr8);

// Work 9

const arr9 = [1, 2, 3, 4, 5];
arr9.splice(2, 0, 10);

console.log(arr9);

// Work 10

const arr10 = [3, 4, 1, 2, 7];
arr10.sort();

console.log(arr10);

// Work 11

const arr11 = ['Привіт,', 'світ', '!'];

console.log(arr11[0] + ' ' + arr11[1] + arr11[2]);

const arr11_1 = arr11.join(' ');

console.log(arr11_1);

// Work 12

const arr12 = ['Привіт,', 'світ', '!'];

arr12[0] = 'Пока,';

console.log(arr12.join(' '));

// Work 13

const arr13 = [1, 2, 3, 4, 5];
const arr13_1 = new Array(1, 2, 3, 4, 5);

console.log(arr13);
console.log(arr13_1);

// Work 14

var arr14 = {
	'ru':['голубой', 'красный', 'зеленый'],
	'en':['blue', 'red', 'green'],
};

console.log(arr14['ru'][0], arr14['en'][0]);

// Work 15

const arr15 = ['a', 'b', 'c', 'd'];

const arr15_1 = [arr15[0], arr15[1]];
const arr15_2 = [arr15[2], arr15[3]];
const arr15_3 = [arr15_1.join(' + '), arr15_2.join(' + ')];

console.log(arr15_3.join(', '))

for (let i = 0; i < arr15.length-2; i++) {
    console.log(arr15[2*i] + '+' + arr15[2*i+1]);
}

// Work 16 - 17

var numberElementsArray = parseInt(prompt("Введіть кількість елементів массиву"));

const arr16 = [];
arr16.length = numberElementsArray;

for (let i = 0; i < arr16.length; i++) {
    arr16[i] = i;
    if (arr16[i] % 2 == 1) {
        document.write("<p class='blue'>" + arr16[i] + "</p>");
    } else {
        document.write("<span class='red'>" + arr16[i] + "</span>");
    }
}

console.log(arr16);

// Work 18

const vegetables = ['Капуста', 'Репа', 'Редиска', 'Морковка'];
const str1 = vegetables.join(',');
console.log(str1);