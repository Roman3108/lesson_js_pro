const DEFAULT_TEXT = "var_text_hello";

function capitalize(text) {
  text = text.split("_");
  let textNew = "";
  for (let i = 0; i < text.length; i++) {
    textNew += text[i][0].toUpperCase() + text[i].slice(1);
  }
  console.log(textNew);

  return textNew;
}

const capitalizeImpruved = (text, seporator = "_") => {
  // Делаем валидацию
  if (!text || !text.length) {
    throw new Error(`Неверный параметр ${text}`);
  }

  // Делаем хуйню
  return text
    .split(seporator)
    .map((splitedItem) => {
      return `${splitedItem.charAt(0).toUpperCase()}${splitedItem.slice(1)}`;
    })
    .join("");
};
const romanFunctionResult = capitalize(DEFAULT_TEXT);
const evgeniyFunctionResult = capitalizeImpruved(DEFAULT_TEXT);

console.log(
  `romanFunctionResult: ${romanFunctionResult}`,
  `evgeniyFunctionResult: ${evgeniyFunctionResult}`
);
