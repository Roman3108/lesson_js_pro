window.onload = () => {


    // WORK 1

    const wrapper = document.createElement('div');
    document.body.append(wrapper);
    wrapper.textContent = '';
    wrapper.style.backgroundColor = 'red';
    wrapper.style.margin = '150px';
    console.log(`Відступи блоку wrapper: ${wrapper.style.margin}`);



    // WORK 2


    const TimerState = {
        Started: 'started',
        Stoped: 'stoped',
        Initial: 'initial'
    }

    let timerState = TimerState.Stoped;



    const scoreboard = document.querySelector('#scoreboard');

    // const buttonStart = document.querySelector('#buttonStart');
    // const buttonStop = document.querySelector('#buttonStop');
    // const buttonReset = document.querySelector('#buttonReset');

    let sec = 0;
    let min = 0;
    let hou = 0;

    const get = id => document.getElementById(id);
   

    const second = document.querySelector('#second');
    second.textContent = sec < 10 ? ('0' + sec) : sec;

    const minute = document.querySelector('#minute');
    minute.textContent = min < 10 ? ('0' + min) : min;

    const hour = document.querySelector('#hour');
    hour.textContent = hou < 10 ? ('0' + hou) : hou;

    
    let startSecond = () => {
        sec++;
        second.innerHTML = sec < 10 ? ('0' + sec) : sec > 59 ? '00' : sec;
        if (sec > 59) {
            sec = 0;
            min++;
            minute.innerHTML = min < 10 ? ('0' + min) : min > 59 ? '00' : min;
            if (min > 59) {
                min = 0;
                hou++;
                hour.innerHTML = hou < 10 ? ('0' + hou) : hou;
            };
        };
        
        
    }
    
    // let startMinute = () => {
    //     min++;
    //     minute.innerHTML = min < 10 ? ('0' + min) : min;
    //     if (min >= 60) { min = 0 };
    // }
    
    // let startHour = () => {
    //     hou++;
    //     hour.innerHTML = hou < 10 ? ('0' + hou) : hou;
    // }

    let reset = () => {
        clearInterval(intervalSecond);
        //clearInterval(intervalMinute);
        //clearInterval(intervalHour);
        second.innerHTML = '00';
        minute.innerHTML = '00';
        hour.innerHTML = '00';
        sec = 0;
        min = 0;
        hou = 0;
    }


    get('buttonStart').onclick = () => {
        if (timerState === TimerState.Started) return;
        timerState = TimerState.Started;
        scoreboard.style.backgroundColor = 'green';
        
        intervalSecond = setInterval(startSecond, 1000);
        //intervalMinute = setInterval(startMinute, 60000);
        //intervalHour = setInterval(startHour, 3600000);
        
    }

    get('buttonStop').onclick = () => {
        timerState = TimerState.Stoped;
        scoreboard.style.backgroundColor = 'red';

        clearInterval(intervalSecond);
        //clearInterval(intervalMinute);
        //clearInterval(intervalHour);
    }

    get('buttonReset').onclick = () => {
        timerState = TimerState.Initial;
        scoreboard.style.backgroundColor = 'gray';
        reset();
    }



    // WORK 3

    const formBox = document.getElementById('form_box');
    formBox.setAttribute('style', 'margin-bottom: 10px')
    const buttonSave = document.getElementById('save');

    
    
    
    buttonSave.onclick = () => {
        let pattern = /\d{3}-\d{3}-\d{2}-\d{2}/;
        let phone = document.getElementById('phone');
        
        
        if (pattern.test(phone.value)) {
            phone.style.borderColor = 'green';
            document.location = 'https://google.com';
        } else {
            phone.style.borderColor = 'red';

            const prompt = document.createElement('div');
            prompt.className = 'prompt_style';
            
            prompt.textContent = 'Не вірно введено номер телефону';
            //phone.value = 'Не вірно введено номер телефону';

            formBox.prepend(prompt);
            
            setTimeout(function(){
                prompt.style.display = 'none';
                phone.style.borderColor = 'black';
            }, 2000);
            
        }        
    }


    // WORK 4

    const image = document.querySelector('#image');
    const img = document.querySelectorAll('.img');

    let nextImg = 0;
    
    let imgShow = () => {

        for (let i = 0; i < img.length; i++) {
            img[i].style.display = 'none';
        }

        nextImg++;

        if (nextImg > img.length) nextImg = 1;
        
        img[nextImg - 1].style.display = 'block';

        img[nextImg - 1].className = 'anim';

        setTimeout(imgShow, 3000);
    };

    imgShow();

    


    
    


    










}