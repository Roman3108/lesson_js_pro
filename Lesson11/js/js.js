window.addEventListener("DOMContentLoaded", () => {
    
    // WORK 1
    console.log('=== №1 ============================');
    
    class User {
        constructor(firstName, lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }

        showUser() {
            const id1 = document.querySelector('#id1');
            id1.innerHTML = `Name: ${this.firstName} and Surname: ${this.lastName}`;
            console.log(`Name: ${this.firstName} and Surname: ${this.lastName}`)
        }
    }

    let user = new User("Leo", "Messi");
    user.showUser();


    // WORK 2
    

    const id2 = document.querySelector('#id2');
    const ul = document.createElement('ul');
    id2.append(ul);

    const li1 = document.createElement('li');
    const li2 = document.createElement('li');
    const li3 = document.createElement('li');
    const li4 = document.createElement('li');
    ul.append(li1);
    ul.append(li2);
    ul.append(li3);
    ul.append(li4);

    li2.previousElementSibling.classList.add('blue');
    li2.nextElementSibling.classList.add('red');


    // WORK 3
    

    const id3 = document.querySelector('#id3');
    const display = document.createElement('span');
    id3.prepend(display);

    id3.addEventListener('mousemove', (e) => {
        //this.innerHTML = ` X: ${e.offsetX}, Y: ${e.offsetY}`;
        display.innerText = `X: ${e.offsetX}, Y: ${e.offsetY}`;
        
        display.setAttribute('style', `position:fixed; left: ${e.clientX + 10}px; top: ${e.clientY + 10}px`);
    })

    id3.addEventListener('mouseout', (e) => {
        display.innerText = ``;
    })


    // WORK 4
    

    const id4 = document.querySelector('#id4');

    const button1 = document.createElement('button');
    button1.classList.add('button');
    button1.addEventListener('mousedown', (e) => {
        button1.innerText = 'buton1';
    })

    const button2 = document.createElement('button');
    button2.classList.add('button');
    button2.addEventListener('mousedown', (e) => {
        button2.innerText = 'buton2';
    })

    const button3 = document.createElement('button');
    button3.classList.add('button');
    button3.addEventListener('mousedown', (e) => {
        button3.innerText = 'buton3';
    })

    const button4 = document.createElement('button');
    button4.classList.add('button');
    button4.addEventListener('mousedown', (e) => {
        button4.innerText = 'buton4';
    })

    id4.append(button1);
    id4.append(button2);
    id4.append(button3);
    id4.append(button4);


    // WORK 5
    

    const id5 = document.querySelector('#id5');
    const box = document.createElement('div');
    box.classList.add('box');
    id5.prepend(box);

    const colors = ['aqua', 'blueviolet', 'gold', 'maroon', 'green'];

    box.addEventListener('mouseover', () => setColor(box));

    //box.addEventListener('mouseout', () => removeColor(box));

    function setColor(element) {
        const color = getRandomColor();
        element.style.backgroundColor = color;

        const size = getRandomNumber(5, 45);
        const { width, height } = id5.getBoundingClientRect();
        const x = getRandomNumber(0, width - size);
        const y = getRandomNumber(0, height - size);


        box.style.width = `${size}px`;
        box.style.height = `${size}px`;
        box.style.top = `${y}px`;
        box.style.left = `${x}px`;
    }

    // function removeColor(element) {
    //     element.style.backgroundColor = 'rgb(77, 72, 72)';
    //     element.style.boxShadow = `0 0 2px #000`;
    // }

    function getRandomColor() {
        const index = Math.floor(Math.random() * colors.length);
        return colors[index];
    }

    function getRandomNumber(min, max) {
        return Math.round(Math.random() * (max - min) + min);
    }


    // WORK 6
    

    const id6 = document.querySelector('#id6');
    const inputColor = document.createElement('input');
    inputColor.type = 'color';
    inputColor.setAttribute('style', `margin-top: 20px; width: 300px;`);
    id6.prepend(inputColor);

    inputColor.addEventListener("change", () => {
        document.body.style.backgroundColor = inputColor.value;
    });



    // WORK 7
    console.log('=== №7 ============================');


    const id7 = document.querySelector('#id7');
    const inputText = document.createElement('input');
    inputText.type = 'text';
    inputText.placeholder = 'Текст для консолі'
    inputText.setAttribute('style', `margin-top: 20px; width: 300px;`);
    id7.prepend(inputText);

    const inputText123 = document.createElement('p');
    id7.append(inputText123);

    inputText.addEventListener("input", (e) => {
        console.log(`Текст з інпуту: ${e.target.value}`);
        //console.log(inputText.value);


        inputText123.textContent = inputText.value;
    });









})