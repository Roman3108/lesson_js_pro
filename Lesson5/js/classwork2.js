

const salary = {
    "Коля": "1000",
    "Вася": "500",
    "Петя": "200",
}

const show = (name) => {
    if (name in salary) {
        document.getElementById("classwork2").innerHTML += `Заробітна платня ${name} становить ${salary[name]} гривень. <br>`;
    } else {
        alert("Такого співробітника немає");
    }
}

show(prompt("Введіть ім'я співробітника"));
show(prompt("Введіть ім'я співробітника"));