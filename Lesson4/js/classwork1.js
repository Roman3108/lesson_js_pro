
// Lesson 1

let weekDay = parseInt(prompt("Введіть день тижня від 1 до 7"));

while (weekDay < 1 || weekDay > 7) {
    weekDay = parseInt(prompt("Введіть день тижня від 1 до 7"));
}

function dayOfWeek(weekDay) {
    switch (weekDay) {
        case 1:
            return console.log("Понеділок");
        case 2:
            return console.log("Вівторок");
        case 3:
            return console.log("Середа");
        case 4:
            return console.log("Четвер");
        case 5:
            return console.log("П'ятниця");
        case 6:
            return console.log("Субота");
        case 7:
            return console.log("Неділя");
    }
}

dayOfWeek(weekDay);

// 2

const randomIntFromInterval = (min, max) => { // min and max included 
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const rndInt = randomIntFromInterval(1, 7)
console.log(rndInt)

const DAYS_MAPPER = ['Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П`ятниця', 'Субота', 'Неділя'];

const dayOfWeekOtherWay = (weekDayNumber) => {
  if(!weekDayNumber || !DAYS_MAPPER[weekDayNumber-1]) {
    throw new Error('Invalid value!');
  }
  
  return DAYS_MAPPER[weekDayNumber-1];
}

const result = dayOfWeekOtherWay(rndInt);

console.log(`dayOfWeekOtherWay: ${result}`);