window.addEventListener('DOMContentLoaded', () => {
    let url = 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json';
    let rec = new XMLHttpRequest();

    rec.open('GET', url);

    rec.addEventListener('readystatechange', () => {
        if (rec.readyState === 4 && rec.status >= 200 && rec.status < 300) {
            showCurrency(JSON.parse(rec.responseText));
        }
    });

    rec.send();

    function showCurrency(arr) {
        arr.forEach(el => {
            const { txt, rate } = el;
            console.log(txt, rate);
            const tr = document.createElement('tr');
            const txttd = document.createElement('td');
            const ratetd = document.createElement('td');
            txttd.innerText = txt;
            ratetd.innerText = rate;
            tr.append(txttd, ratetd);
            document.querySelector('tbody').append(tr);
        });
    };


    // let url = 'https://swapi.dev/api/planets';

    // // створимо запит на сервер
    // const data = fetch(url, {method: 'GET'});

    // // Робота з запитом, оброблюємо як Promise
    // const datathen = data.then((res) => res.json(), (error) => console.log(error));

    // datathen.then((res1) => {
    //     //document.write(res1.results);
    //     console.log(res1);
    // })



});