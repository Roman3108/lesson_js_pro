window.onload = () => {
    
    const button = document.getElementById('writeCircle');
    button.onclick = function () {
        const div = document.getElementById('wrapper');

        const input = document.createElement('input');
        input.setAttribute('placeholder', 'Введіть діаметр кола');
        input.classList.add('input_diametr');
        div.append(input);

        const createButton = document.createElement('button');
        createButton.type = 'button';
        createButton.classList.add('writeCircle');
        createButton.textContent = 'Намалювати';
        div.append(createButton);

        createButton.onclick = function () {
            const dataDiametr = document.querySelector('input').value;
            const divByCircle = document.createElement('div');
            divByCircle.style.display = 'flex';
            divByCircle.style.flexWrap = 'wrap';
            divByCircle.style.width = '80%';

            div.append(divByCircle);

            let count = prompt('Введіть кількість кіл');

            for (let i = 0; i <= count; i++) {
                const circle = document.createElement('div');
                circle.className = 'circle';
                circle.style.width = `${dataDiametr}px`; //
                circle.style.height = `${dataDiametr}px`; //
                circle.style.borderRadius = '50%';
                circle.style.margin = '5px';
                circle.style.display = 'inline-flex';
                circle.style.backgroundColor = `rgb(${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)}, ${Math.floor(Math.random() * 255)})`;
                divByCircle.prepend(circle);


                input.remove();
                createButton.remove();

                const [...circles] = document.getElementsByClassName('circle');

                circles.forEach(elem => {
                    elem.onclick = function () {
                        elem.remove()
                    } 
                });
            }

            

        }
        
    }
    







}