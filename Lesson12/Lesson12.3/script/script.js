

localStorage.user = JSON.stringify([]);

class User {
    constructor(firstName, lastName, age, phoneNumber, index) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.phoneNumber = phoneNumber;
        this.index = index;
        this.status = true;
    };
};

const createUserList = () => {
    // отримуємо массив інфо з пам'яті
    let currentUsers = JSON.parse(localStorage.user);
    currentUsers.forEach(el => {
        let tr = document.createElement('tr');
        let tdFirstName = document.createElement('td');
        let tdLastName = document.createElement('td');
        let tdAge = document.createElement('td');
        let tdPhoneNumber = document.createElement('td');
        let tdIndex = document.createElement('td');
        let tdStatus = document.createElement('td');
        let inputCheckbox = document.createElement('input');

        inputCheckbox.type = 'checkbox';
        inputCheckbox.checked = el.status;
        tdFirstName.innerText = el.firstName;
        tdLastName.innerText = el.lastName;
        tdAge.innerText = el.age;
        tdPhoneNumber.innerText = el.phoneNumber;
        tdIndex.innerText = el.index;

        tdStatus.append(inputCheckbox);
        tr.append(tdFirstName, tdLastName, tdAge, tdPhoneNumber, tdIndex, tdStatus);
        document.querySelector('tbody').append(tr);
    })
}

let [...allInputs] = document.querySelectorAll('input');

let inputRes = allInputs.map(el => {
    //console.log(el);
    return el;
});

const validate = (target) => {
    //console.log(target);
    // if (target.id === 'firstName') {
    //     // let pattern = /^[A-z]{2,}$/i;
    //     // return pattern.test(target.value);
    //     return /^[A-z]{2,}$/i.test(target.value);
    // }
    switch (target.id) {
        case 'firstName': return /^[A-z]{2,}$/i.test(target.value);
        case 'lastName': return /^[А-я]{2,}$/i.test(target.value);
        case 'age': return /^\d{1,2}$/.test(target.value);
        case 'phoneNamber': return /^\+380\d{9}$/.test(target.value);
        case 'index': return /^\d{5}$/.test(target.value);
        default: throw new Error('Невірний виклик регулярного виразу');
    };
}
  
    
inputRes.forEach(el => {
    el.addEventListener('change', (event) => {
        //console.log(validate(event.target));
        validate(event.target);
    })
});

let saveButton = document.querySelector('[type=button]');
//console.log(saveButton);

saveButton.addEventListener('click', () => {
    // функція валідації після натиску кнопки
    let validateRes = inputRes.map(el => {
        return validate(el);
    });
    //console.log(res);
    // перевірка чи немає помилок в інпутах
    if (!validateRes.includes(false)) {
        // якщо немає помилок, записуємо данні в локалСторедж
        let a = JSON.parse(localStorage.user);
        a.push(new User(...inputRes.map(el => {
            return el.value;
        })));
        localStorage.user = JSON.stringify(a);
        inputRes.forEach(el => el.value = '');
        createUserList();
    } // тут вивід помилок користувачу
});