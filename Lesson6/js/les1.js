
//const date = new Date();


const teams = [
    {
        name: 'Мілан',
        city: 'Мілан',
        country: 'Італія',
        titulChampionsLeague: 7
    },
    {
        name: 'Реал',
        city: 'Мадрид',
        country: 'Іспанія',
        titulChampionsLeague: 14
    },
    {
        name: 'Баварія',
        city: 'Мюнхен',
        country: 'Німеччина',
        titulChampionsLeague: 6
    },
];   

function Team(obj1) {
    this.name = obj1.name;
    this.city = obj1.city;
    this.country = obj1.country;
    this.titulChampionsLeague = obj1.titulChampionsLeague;
}


Team.prototype.showAdress = () => {
    return `adress: ${this.city}, ${this.country}`;
}
 

const teamsArray = teams.map((item) => new Team(item));

const sortByName = (array) => {
    return array.sort((a, b) => a.name == b.name ? 0 : (a.name < b.name ? -1 : 1));
}

const sortByTitulChampionsLeagueUp = (array) => {
    return array.sort((a, b) => b.titulChampionsLeague - a.titulChampionsLeague);
}

const sortByTitulChampionsLeagueDone = (array) => {
    return array.sort((a, b) => a.titulChampionsLeague - b.titulChampionsLeague);
}


const sortedByName = sortByName(teamsArray);
console.log(`sortedByName: ${JSON.stringify(sortedByName)}`);


const sortedByTitulChampionsLeagueUp = sortByTitulChampionsLeagueUp(teamsArray);
console.log(`sortedByTitulChampionsLeagueUp: ${JSON.stringify(sortedByTitulChampionsLeagueUp)}`);

const sortedByTitulChampionsLeagueDone = sortByTitulChampionsLeagueDone(teamsArray);
console.log(`sortedByTitulChampionsLeagueDone: ${JSON.stringify(sortedByTitulChampionsLeagueDone)}`);




//const liverpool = new Team('Ліверпуль', 'Ліверпуль', 'Англія', 6);
//const barcelona = new Team('Барселона', 'Барселона', 'Іспанія', 5);
//const ajax = new Team('Аякс', 'Амстердам', 'Нідерланди', 4);
//const inter = new Team('Інтер', 'Мілан', 'Італія', 3);
//const manUtd = new Team('Манчестер Юнайтед', 'Манчестер', 'Англія', 3);
//const juve = new Team('Ювентус', 'Турин', 'Італія', 2);
//const benfica = new Team('Бенфіка', 'Ліссабон', 'Португаля', 2);


//teams.push(liverpool);

//console.log(teams)


        
const showTeams = teams
        .map((item) => {
            return `<tr>
                    <td><div>${item.name}</div></td>
                    <td><div>${item.city}</div></td>
                    <td><div>${item.country}</div></td>
                    <td><div>${item.titulChampionsLeague}</div></td>
                </tr>`
        })
        .join('<br>');

const show1 = () => {
    document.getElementById('title').innerHTML = `Таблиця переможців Ліги Чемпіонів`
    document.getElementById('tbody').innerHTML = `${showTeams}`;
}

//show1();



const showSortedByName = sortedByName
        .map((item) => {
            return `<tr>
                    <td><div>${item.name}</div></td>
                    <td><div>${item.city}</div></td>
                    <td><div>${item.country}</div></td>
                    <td><div>${item.titulChampionsLeague}</div></td>
                </tr>`
        })
        .join('<br>');

const show2 = () => {
    document.getElementById('title').innerHTML = `Таблиця переможців Ліги Чемпіонів`
    document.getElementById('tbody').innerHTML = `${showSortedByName}`;
}

show2();



// Чомусь ця сортировка не працює
const showSortedByTitulChampionsLeagueUp = sortedByTitulChampionsLeagueUp
        .map((item) => {
            return `<tr>
                    <td><div>${item.name}</div></td>
                    <td><div>${item.city}</div></td>
                    <td><div>${item.country}</div></td>
                    <td><div>${item.titulChampionsLeague}</div></td>
                </tr>`
        })
        .join('<br>');

const show3 = () => {
    document.getElementById('title').innerHTML = `Таблиця переможців Ліги Чемпіонів`
    document.getElementById('tbody').innerHTML = `${showSortedByTitulChampionsLeagueUp}`;
}

//show3();



// Чомусь ця сортировка не працює
const showSortedByTitulChampionsLeagueDone = sortedByTitulChampionsLeagueDone
        .map((item) => {
            return `<tr>
                    <td><div>${item.name}</div></td>
                    <td><div>${item.city}</div></td>
                    <td><div>${item.country}</div></td>
                    <td><div>${item.titulChampionsLeague}</div></td>
                </tr>`
        })
        .join('<br>');

const show4 = () => {
    document.getElementById('title').innerHTML = `Таблиця переможців Ліги Чемпіонів`
    document.getElementById('tbody').innerHTML = `${showSortedByTitulChampionsLeagueDone}`;
}

//show4();

