


const wallet = {
    name: "Roman",
    bitcoin: {
        name: "Bitcoin",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
        price: 20746.57,
        coin: 23,
    },
    ethereum: {
        name: "Ethereum",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
        price: 1185.38,
        coin: 56,
    },
    stellar: {
        name: "Stellar",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
        price: 0.1182,
        coin: 189,
    },
    show: function() {
        document.getElementById("header").innerHTML = `Доброго дня, ${wallet.name}`;

        document.getElementById("btn_logo").innerHTML = `${wallet.bitcoin.logo}`;
        document.getElementById("btn_name").innerHTML = `${wallet.bitcoin.name}`;
        document.getElementById("btn_price").innerHTML = `$${wallet.bitcoin.price}`;
        document.getElementById("btn_coin").innerHTML = `${wallet.bitcoin.coin}`;
        document.getElementById("btn_sum").innerHTML = `$${(wallet.bitcoin.coin * wallet.bitcoin.price).toFixed(2)}`;

        document.getElementById("eth_logo").innerHTML = `${wallet.ethereum.logo}`;
        document.getElementById("eth_name").innerHTML = `${wallet.ethereum.name}`;
        document.getElementById("eth_price").innerHTML = `$${wallet.ethereum.price}`;
        document.getElementById("eth_coin").innerHTML = `${wallet.ethereum.coin}`;
        document.getElementById("eth_sum").innerHTML = `$${(wallet.ethereum.coin * wallet.ethereum.price).toFixed(2)}`;

        document.getElementById("stl_logo").innerHTML = `${wallet.stellar.logo}`;
        document.getElementById("stl_name").innerHTML = `${wallet.stellar.name}`;
        document.getElementById("stl_price").innerHTML = `$${wallet.stellar.price}`;
        document.getElementById("stl_coin").innerHTML = `${wallet.stellar.coin}`;
        document.getElementById("stl_sum").innerHTML = `$${(wallet.stellar.coin * wallet.stellar.price).toFixed(2)}`;
    }
}

wallet.show();

