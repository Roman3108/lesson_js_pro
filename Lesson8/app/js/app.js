

window.onload = () => {
    const wrapper = document.getElementById('appBox');

    // Scoreboard
    
    const scoreboard = document.createElement('input');
    scoreboard.setAttribute('style', 'min-width: 150px; padding: 5px');
    wrapper.append(scoreboard);

    // ListSign

    const listSign = document.createElement('ul');
    listSign.setAttribute('style', `margin-bottom: 20px; 
                                    list-style-type: none;
                                    padding: 0px`);
    wrapper.append(listSign);

    // Addition

    const liAddition = document.createElement('li');
    listSign.append(liAddition);
    const additionButton = document.createElement('button');
    liAddition.append(additionButton);
    additionButton.textContent = 'Я додаю +'
    additionButton.value = '+';

    // Subtraction

    const liSubtraction = document.createElement('li');
    listSign.append(liSubtraction);
    const subtractionButton = document.createElement('button');
    liSubtraction.append(subtractionButton);
    subtractionButton.textContent = 'Я додаю -'
    subtractionButton.value = '-';

    // Multiply

    const liMultiply = document.createElement('li');
    listSign.append(liMultiply);
    const multiplyButton = document.createElement('button');
    liMultiply.append(multiplyButton);
    multiplyButton.textContent = 'Я додаю *'
    multiplyButton.value = '*';

    // Division

    const liDivision = document.createElement('li');
    listSign.append(liDivision);
    const divisionButton = document.createElement('button');
    liDivision.append(divisionButton);
    divisionButton.textContent = 'Я додаю /'
    divisionButton.value = '/';

    // ListNumber

    const listNumber = document.createElement('ul');
    listNumber.setAttribute('style', `margin-bottom: 20px; 
                                    list-style-type: none;
                                    padding: 0px`);
    wrapper.append(listNumber);

    // 1

    const liOne = document.createElement('li');
    listNumber.append(liOne);
    const oneButton = document.createElement('button');
    liOne.append(oneButton);
    oneButton.textContent = 'Я додаю 1'
    oneButton.value = '1';

    // 2

    const liTwo = document.createElement('li');
    listNumber.append(liTwo);
    const twoButton = document.createElement('button');
    liTwo.append(twoButton);
    twoButton.textContent = 'Я додаю 2'
    twoButton.value = '2';

    // 3

    const liThree = document.createElement('li');
    listNumber.append(liThree);
    const threeButton = document.createElement('button');
    liThree.append(threeButton);
    threeButton.textContent = 'Я додаю 3'
    threeButton.value = '3';

    // 4

    const liFour = document.createElement('li');
    listNumber.append(liFour);
    const fourButton = document.createElement('button');
    liFour.append(fourButton);
    fourButton.textContent = 'Я додаю 4'
    fourButton.value = '4';

    // 5

    const liFifth = document.createElement('li');
    listNumber.append(liFifth);
    const fifthButton = document.createElement('button');
    liFifth.append(fifthButton);
    fifthButton.textContent = 'Я додаю 5'
    fifthButton.value = '5';

    // 6

    const liSix = document.createElement('li');
    listNumber.append(liSix);
    const sixButton = document.createElement('button');
    liSix.append(sixButton);
    sixButton.textContent = 'Я додаю 6'
    sixButton.value = '6';

    // 7

    const liSeven = document.createElement('li');
    listNumber.append(liSeven);
    const sevenButton = document.createElement('button');
    liSeven.append(sevenButton);
    sevenButton.textContent = 'Я додаю 7'
    sevenButton.value = '7';

    // 8

    const liEight = document.createElement('li');
    listNumber.append(liEight);
    const eightButton = document.createElement('button');
    liEight.append(eightButton);
    eightButton.textContent = 'Я додаю 8'
    eightButton.value = '8';

    // 9

    const liNine = document.createElement('li');
    listNumber.append(liNine);
    const nineButton = document.createElement('button');
    liNine.append(nineButton);
    nineButton.textContent = 'Я додаю 9'
    nineButton.value = '9';

    // 0

    const liZero = document.createElement('li');
    listNumber.append(liZero);
    const zeroButton = document.createElement('button');
    liZero.append(zeroButton);
    zeroButton.textContent = 'Я додаю 0'
    zeroButton.value = '0';

    // Function

    let [...buttons] = document.querySelectorAll('button');
    
    buttons.forEach(el => el.onclick = function () {
        scoreboard.value += el.value; 
    })
    
    
    // buttons.onclick = function () {
    //     let temp = input1.value;
    //     input1.value = input2.value;
    //     input2.value = temp;
  
    // }
}